import { Marker } from './classes.js';
import { FullMarker } from './classes.js';

const input = document.querySelector('.input');

let counter = 10;

let massage = new Marker(input.value, 'blue', counter),
    full = new FullMarker();

const getInput = () => {
    let symbol = input.value.split('').pop();

    if(symbol !== ' ') {
        counter -= 0.5;
    }

    if(counter <= 0) {
        counter = 0;
        input.value = input.value.replace(/\S/g, '');
    }
};

input.addEventListener('input', getInput);

massage.print();
full.ink();



  