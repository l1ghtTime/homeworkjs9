const input = document.querySelector('.input');

export class Marker {
    constructor(str, color, amount) {
      this.str = str;
      this.color = color;
      this.amount = amount;
    } 
    
    print() {
      input.value = this.str;
      input.style.color = this.color;
    }
}
  
export class FullMarker extends Marker {
  ink() {
    this.amount = 10;
  }
}

